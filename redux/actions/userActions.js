import axios from 'axios';
import { GET_LIST_USER } from '../constants/user';

const getListUser = () => {
  return async dispatch => {
    const respone = await axios({
      url: 'https://jsonplaceholder.typicode.com/users',
      method: 'GET'
    });
    //Update store
    dispatch({ type: GET_LIST_USER, data: respone.data });
  };
};

const getTotalPost = async () => {
  try {
    const respone = await axios({
      url: 'https://jsonplaceholder.typicode.com/posts',
      method: 'GET'
    });
    return { ok: true, data: respone.data.length };
  } catch (err) {
    return { ok: false, data: err };
  }
};

export default {
  getListUser,
  getTotalPost
};
