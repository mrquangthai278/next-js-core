import userActions from './userActions';

const actions = {
  user: userActions
};

export default actions;
