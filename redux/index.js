import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadStateFromLocal, saveStateToLocal } from '../plugins/localStorage';
import { rootReducer } from './reducers/rootReducer';
import thunk from 'redux-thunk';

const persistedState = loadStateFromLocal();

export const initializeStore = () => {
  const store = createStore(
    rootReducer,
    persistedState,
    applyMiddleware(thunk)
  );
  store.subscribe(() => saveStateToLocal(store.getState()));
  return store;
};
