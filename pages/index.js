import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { withRedux } from '../plugins/redux';
import UserList from '../components/UserList';
import MenuHeader from '../components/MenuHeader';
import actions from '../redux/actions';

const IndexPage = () => {
  const dispatch = useDispatch();

  const states = {
    countPost: 0
  };
  const [data, setData] = useState(states);

  const initTotalPost = async () => {
    const resPost = await actions.user.getTotalPost();
    if (resPost.ok) {
      setData({
        ...data,
        countPost: resPost.data
      });
    }
  };

  useEffect(() => {
    dispatch(actions.user.getListUser());
    initTotalPost();
  }, []);

  return (
    <>
      <MenuHeader />
      <UserList />
      <h1>Total: {data.countPost} post</h1>
    </>
  );
};

IndexPage.getInitialProps = ({ reduxStore }) => {
  const { dispatch } = reduxStore;
  return {};
};

export default withRedux(IndexPage);
