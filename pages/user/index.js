import React from 'react';
import { useDispatch } from 'react-redux';
import { withRedux } from '../../plugins/redux';
import FormData from '../../components/shared/FormData';

const UserPage = () => {
  return (
    <>
      <FormData />
    </>
  );
};

UserPage.getInitialProps = ({ reduxStore }) => {
  const { dispatch } = reduxStore;
  return {};
};

export default withRedux(UserPage);
