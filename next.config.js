const withSass = require('@zeit/next-sass');

module.exports = withSass({
  env: {
    enviroment: 'production'
  }
});
