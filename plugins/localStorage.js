export const loadStateFromLocal = () => {
  try {
    const serializedState = localStorage.getItem('redux');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    console.log('loadStateFromLocal -> err', err);
    return undefined;
  }
};

export const saveStateToLocal = state => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('redux', serializedState);
  } catch (err) {
    console.log('err', err);
  }
};
