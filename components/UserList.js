import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import './UserList.scss';

const UserItem = props => {
  return (
    <div className='user-item'>
      <p>{props.item.name}</p>
    </div>
  );
};

const UserList = () => {
  const listUser = useSelector(state => state.user.listUser);
  return (
    <Fragment>
      {listUser.map(item => {
        return <UserItem item={item} key={item.id} />;
      })}
    </Fragment>
  );
};

export default UserList;
