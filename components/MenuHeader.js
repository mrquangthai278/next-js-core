import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import './MenuHeader.scss';

const MenuHeader = () => {
  const router = useRouter();

  const handlePush = url => {
    router.push(url);
  };

  const listMenu = [
    {
      name: 'Index',
      url: '/'
    },
    {
      name: 'About us',
      url: '/about-us'
    },
    {
      name: 'Login',
      url: '/user'
    }
  ];

  return (
    <div className='wrap-menu'>
      {listMenu.map(item => {
        return (
          <span
            className='menu-item'
            key={item.name}
            onClick={() => {
              handlePush(item.url);
            }}>
            {item.name}
          </span>
        );
      })}
    </div>
  );
};

export default MenuHeader;
