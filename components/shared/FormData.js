import React, { Component, useState } from 'react';
import { useRouter } from 'next/router';
import './FormData.scss';

const FormData = props => {
  const router = useRouter();
  const initialState = {
    email: '',
    password: ''
  };

  const [data, setData] = useState(initialState);

  const submitFormData = () => {
    console.log('submitFormData', data);
  };

  const handleBack = () => {
    router.push('/');
  };

  const handleChangeInput = event => {
    setData({
      ...data,
      [event.target.name]: event.target.value
    });
  };

  return (
    <div className='wrap-form'>
      <p>Email</p>
      <input type='text' name='email' onChange={handleChangeInput}></input>
      <p>Password</p>
      <input
        type='password'
        name='password'
        onChange={handleChangeInput}></input>
      <p>
        <button onClick={submitFormData}>Submit</button>
        <button onClick={handleBack}>Back</button>
      </p>
    </div>
  );
};

export default FormData;
